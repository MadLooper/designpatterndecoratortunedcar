package main.java;

/**
 * Created by RENT on 2017-08-14.
 */
public class CarShop {

    public ICar addExtraCharger(ICar c) {
        boolean charger = true;
        if (c.isHasCharger()) {
            charger = false;
        }
        return new TunedCar.Builder().setTunedCar(c).setPressureAdded(20).setSetCharger(charger).setHorsePowerMultiplier(10).setEngineCapacityMultiplier(1).createTunedCar();
    }

    public ICar addExtraEngine(ICar c) {
        return new TunedCar.Builder().setTunedCar(c).setHorsePowerMultiplier(20).setEngineCapacityMultiplier(20).createTunedCar();
    }

    public ICar addSeats(ICar c) {
        return new TunedCar.Builder().setTunedCar(c).setHorsePowerMultiplier(0.5).setEngineCapacityMultiplier(1).createTunedCar();
    }
}
