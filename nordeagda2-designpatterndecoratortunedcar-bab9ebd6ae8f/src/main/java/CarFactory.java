package main.java;

/**
 * Created by RENT on 2017-08-14.
 */
public class CarFactory extends Car {

    public CarFactory(double chargerPressure, double engineCapacity, double horsePower, boolean hasCharger, int seatsNumber) {
        super(chargerPressure, engineCapacity, horsePower, hasCharger, seatsNumber);
    }

    public static ICar createShitCar() {
        return new Car(23, 50, 200, false, 4);
    }

    public static ICar crateMidCar() {
        return new Car(45, 70, 300, true, 2);
    }

    public static ICar crateAwsomeCar() {
        return new Car(67, 120, 500, true, 2);
    }
}
