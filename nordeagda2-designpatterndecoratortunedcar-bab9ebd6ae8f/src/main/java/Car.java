package main.java;

/**
 * Created by RENT on 2017-08-14.
 */
public class Car implements ICar {
    private double chargerPressure, engineCapacity, horsePower;
    private boolean hasCharger;
    private int seatsNumber;

    public Car(double chargerPressure, double engineCapacity, double horsePower, boolean hasCharger, int seatsNumber) {
        this.chargerPressure = chargerPressure;
        this.engineCapacity = engineCapacity;
        this.horsePower = horsePower;
        this.hasCharger = hasCharger;
        this.seatsNumber = seatsNumber;
    }

    public double getChargerPressure() {
        return chargerPressure;
    }

    public void setChargerPressure(double chargerPressure) {
        this.chargerPressure = chargerPressure;
    }

    public double getEngineCapacity() {
        return engineCapacity;
    }

    @Override
    public ICar getCar() {
        return this;
    }

    public void setEngineCapacity(double engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public double getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(double horsePower) {
        this.horsePower = horsePower;
    }

    public boolean isHasCharger() {
        return hasCharger;
    }

    public void setHasCharger(boolean hasCharger) {
        this.hasCharger = hasCharger;
    }

    public int getSeatsNumber() {
        return seatsNumber;
    }

    public void setSeatsNumber(int seatsNumber) {
        this.seatsNumber = seatsNumber;
    }

    @Override
    public String toString() {
        return
                "chargerPressure: " + chargerPressure +
                ", engineCapacity: " + engineCapacity +
                ", horsePower: " + horsePower +
                ", hasCharger: " + hasCharger +
                ", seatsNumber: " + seatsNumber;

    }
}
