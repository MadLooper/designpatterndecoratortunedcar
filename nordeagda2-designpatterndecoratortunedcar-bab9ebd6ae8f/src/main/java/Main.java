package main.java;

/**
 * Created by RENT on 2017-08-14.
 */
public class Main {
    public static void main(String[] args) {
        ICar carAwsome = CarFactory.crateAwsomeCar();
        ICar carMid = CarFactory.crateMidCar();
        ICar carShit = CarFactory.createShitCar();

        CarShop carShop = new CarShop();
        ICar tunedCarMid = carShop.addSeats(carMid);
        ICar tunedCarShit = carShop.addExtraCharger(carShit);
        ICar tunedCarAwsome = carShop.addExtraEngine(carAwsome);

        System.out.println(carAwsome);
        System.out.println(tunedCarAwsome);
        System.out.println();
        System.out.println(carMid);
        System.out.println(tunedCarMid);
        System.out.println();
        System.out.println(carShit);
        System.out.println(tunedCarShit);

    }
}
