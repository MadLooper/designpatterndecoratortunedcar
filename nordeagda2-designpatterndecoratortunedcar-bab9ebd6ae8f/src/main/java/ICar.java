package main.java;

/**
 * Created by RENT on 2017-08-14.
 */
public interface ICar {
    double getHorsePower();
    boolean isHasCharger();
    double getEngineCapacity();
    double getChargerPressure();
    ICar getCar();

}
